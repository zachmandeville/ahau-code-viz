const path = require('path')

module.exports = function flatRepos (input, pathSoFar = '') {
  if (typeof input === 'string') {
    return [{
      path: path.join(pathSoFar, input.split('/').pop()),
      src: toGitRepo(input)
    }]
  }

  pathSoFar = input.dir
    ? path.join(pathSoFar, input.dir)
    : path.join(pathSoFar, input.repo.split('/').pop())

  let results = []
  if (input.repo) {
    results.push({
      path: pathSoFar,
      src: toGitRepo(input.repo)
    })
  }

  if (input.nested) {
    for (const _input of input.nested) {
      results = [
        ...results,
        ...(flatRepos(_input, pathSoFar))
      ]
    }
  }

  return results
}

function toGitRepo (location) {
  return `git@gitlab.com:${location}.git`
}
