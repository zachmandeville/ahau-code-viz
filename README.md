# ahau-code-viz

This repo uses [gource] to visualise ahau's development history.

We used this [visualising multiple repos][wiki-guide] guide,
and Node scripting to weave together the git history of all the parts of this project.


## Setup

You need `git`, `node`, and `gource`

```bash
$ npm install
$ npm start
```

To update the data, run `npm run build`



[gource]: https://gource.io/
[wiki-guide]: https://github.com/acaudwell/Gource/wiki/Visualizing-Multiple-Repositories
