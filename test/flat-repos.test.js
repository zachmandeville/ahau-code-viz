const test = require('tape')
const flat = require('../flat-repos')

test('flat-repos', t => {
  const repos = {
    dir: 'build/ahau',
    repo: 'ahau/ahau',
    nested: [
      {
        dir: 'desktop/plugins/ssb-ahau', // relative to parent
        repo: 'ahau/lib/ssb-plugins/ssb-ahau',
        nested: [
          'ahau/lib/graphql/ssb-graphql-main'
        ]
      },
      {
        dir: 'desktop/plugins/ssb-ahau/modules', // relative to parent
        nested: [
          {
            dir: '@ssb-graphql/profile',
            repo: 'ahau/lib/graphql/ssb-graphql-profile'
          },
          'ahau/lib/graphql/ssb-graphql-whakapapa'
        ]
      }
    ]
  }

  const expected = [
    {
      path: 'build/ahau',
      src: 'git@gitlab.com:ahau/ahau.git'
    },
    {
      path: 'build/ahau/desktop/plugins/ssb-ahau',
      src: 'git@gitlab.com:ahau/lib/ssb-plugins/ssb-ahau.git'
    },
    {
      path: 'build/ahau/desktop/plugins/ssb-ahau/ssb-graphql-main',
      src: 'git@gitlab.com:ahau/lib/graphql/ssb-graphql-main.git'
    },
    {
      path: 'build/ahau/desktop/plugins/ssb-ahau/modules/@ssb-graphql/profile',
      src: 'git@gitlab.com:ahau/lib/graphql/ssb-graphql-profile.git'
    },
    {
      path: 'build/ahau/desktop/plugins/ssb-ahau/modules/ssb-graphql-whakapapa',
      src: 'git@gitlab.com:ahau/lib/graphql/ssb-graphql-whakapapa.git'
    }
  ]

  t.deepEqual(flat(repos), expected)

  t.end()
})
