const test = require('tape')
const complete = require('../complete-repos')

test('complete-repos', t => {
  const repos = {
    dir: 'build/ahau',
    repo: 'ahau/ahau',
    nested: [
      {
        dir: 'desktop/plugins/ssb-ahau', // relative to parent
        repo: 'ahau/lib/ssb-plugins/ssb-ahau',
        nested: [
          'ahau/lib/graphql/ssb-graphql-main'
        ]
      },
      {
        dir: 'desktop/plugins/ssb-ahau/modules', // relative to parent
        nested: [
          {
            dir: '@ssb-graphql/profile',
            repo: 'ahau/lib/graphql/ssb-graphql-profile'
          },
          'ahau/lib/graphql/ssb-graphql-whakapapa'
        ]
      }
    ]
  }

  const expected = {
    path: 'build/ahau',
    src: 'git@gitlab.com:ahau/ahau.git',
    nested: [
      {
        path: 'build/ahau/desktop/plugins/ssb-ahau',
        src: 'git@gitlab.com:ahau/lib/ssb-plugins/ssb-ahau.git',
        nested: [
          {
            path: 'build/ahau/desktop/plugins/ssb-ahau/ssb-graphql-main',
            src: 'git@gitlab.com:ahau/lib/graphql/ssb-graphql-main.git',
            nested: []
          }
        ]
      },
      {
        path: 'build/ahau/desktop/plugins/ssb-ahau/modules', // relative to parent
        src: undefined,
        nested: [
          {
            path: 'build/ahau/desktop/plugins/ssb-ahau/modules/@ssb-graphql/profile',
            src: 'git@gitlab.com:ahau/lib/graphql/ssb-graphql-profile.git',
            nested: []
          },
          {
            path: 'build/ahau/desktop/plugins/ssb-ahau/modules/ssb-graphql-whakapapa',
            src: 'git@gitlab.com:ahau/lib/graphql/ssb-graphql-whakapapa.git',
            nested: []
          }
        ]
      }
    ]
  }

  t.deepEqual(complete(repos), expected)

  t.end()
})
