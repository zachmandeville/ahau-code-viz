module.exports = {
  repo: 'ahau/ahau', // assumes gitlab
  dir: 'build/ahau',
  nested: [
    {
      dir: 'desktop/lib/ssb-crut',
      repo: 'ahau/lib/ssb-crut',
      nested: [
        {
          dir: 'modules',
          nested: [
            'tangle-js/tangle-graph',
            'tangle-js/tangle-reduce',
            'tangle-js/strategies/strategy',
            'tangle-js/strategies/overwrite',
            'tangle-js/strategies/simple-set',
            'tangle-js/strategies/complex-set',
            'tangle-js/strategies/linear-append',
            'tangle-js/strategies/tangle-overwrite-fields'
          ]
        }
      ]
    },
    {
      dir: 'desktop/lib/ssb-migrate',
      repo: 'ahau/lib/ssb-migrate'
    },
    {
      dir: 'desktop/plugins',
      nested: [
        {
          dir: 'ssb-ahau',
          repo: 'ahau/lib/ssb-plugins/ssb-ahau',
          nested: [{
            dir: '@ssb-graphql',
            nested: [
              'main',
              'artefact',
              'invite',
              'tribes',
              'profile',
              'settings',
              'story',
              'whakapapa'
            ].map(name => {
              return {
                dir: name,
                src: `ahau/lib/graphql/ssb-graphql-${name}`
              }
            })
          }]
        },
        'ahau/lib/ssb-plugins/ssb-artefact',
        'ahau/lib/ssb-plugins/ssb-hyper-blobs',
        'ahau/lib/ssb-plugins/ssb-profile',
        'ahau/lib/ssb-plugins/ssb-settings',
        'ahau/lib/ssb-plugins/ssb-story',
        {
          dir: 'ssb-tribes',
          repo: 'git@github.com:ssbc/ssb-tribes.git',
          nested: [
            {
              dir: 'modules',
              nested: [
                'ahau/lib/ssb-keyring',
                'git@github.com:ssbc/envelope-js.git',
                'git@github.com:ssbc/ssb-private-group-keys.git'
              ]
            }
          ]
        },
        'ahau/lib/ssb-plugins/ssb-tribes-registration',
        'ahau/lib/ssb-plugins/ssb-whakapapa'
      ]
    },
    'ahau/pataka'
  ]
}
